package com.brijeshsonawane234.music.Activity;

import androidx.appcompat.app.AppCompatActivity;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.brijeshsonawane234.music.Api.ApiInterface;
import com.brijeshsonawane234.music.Pojo.Data;
import com.brijeshsonawane234.music.Pojo.Image;
import com.brijeshsonawane234.music.Pojo.Json;
import com.brijeshsonawane234.music.Pojo.ObjectData;
import com.brijeshsonawane234.music.Pojo.PostData;
import com.brijeshsonawane234.music.R;
import com.bumptech.glide.Glide;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    ImageView imageView1,imageView2,imageView3;
    TextView textView,textView1,textView2;
    List<Image> image;
    List<Data> data;
    int count=0;
    String imgRedirct1,imgRedirct2,imgRedirct3;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        imageView1=findViewById(R.id.imageView1);
        imageView2=findViewById(R.id.imageView2);
        imageView3=findViewById(R.id.imageView3);
        textView=findViewById(R.id.tv3);
        textView1=findViewById(R.id.tv1);
        textView2=findViewById(R.id.tv2);

        Retrofit retrofit=new Retrofit.Builder()
                .baseUrl("https://prod-b2c-api.taglr.com:8443/web-b2c/open/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        PostData postData=new PostData(false,"dc63df2df84f4264");
        ApiInterface apiInterface=retrofit.create(ApiInterface.class);
        Call<Json> call=apiInterface.postInfo(postData);

        call.enqueue(new Callback<Json>() {
            @Override
            public void onResponse(Call<Json> call, Response<Json> response)
            {
                Log.d("data","pass");
                Json json=response.body();
                List<ObjectData> objectData=json.getObjectData();
                for (ObjectData o:objectData)
                {
                    Log.d("section header",o.getSection_header());
                    data=o.getData();
                    for (Data d: data)
                    {

                        if(d.getImages()!=null)
                        {
                            Log.d("data1",""+d.getImages().size());

                            if (o.getSection_header().equals("Music on My Mind! "))
                            {
                                textView.setText(o.getSection_header());
                                image = d.getImages();
                                if(count==0)
                                {
                                    Glide.with(MainActivity.this).load(image.get(0).getImage_url()).into(imageView1);
                                    textView1.setText(d.getLabel().toUpperCase());
                                    imgRedirct1=image.get(0).getRedirect_url();
                                }
                                else
                                {
                                    if (count == 1)
                                    {
                                        Glide.with(MainActivity.this).load(image.get(0).getImage_url()).into(imageView2);
                                        textView2.setText(d.getLabel().toUpperCase());
                                        imgRedirct2=image.get(0).getRedirect_url();
                                    }
                                    else
                                    {
                                        Glide.with(MainActivity.this).load(image.get(0).getImage_url()).into(imageView3);
                                        imgRedirct3=image.get(0).getRedirect_url();
                                    }
                                }
                                count++;
                            }
                        }

                    }
                }

            }

            @Override
            public void onFailure(Call<Json> call, Throwable t)
            {
                Log.d("data","faill");
            }
        });

    }

    public void fun1(View view)
    {
        Log.d("function","fun1");
        Intent viewIntent =
                new Intent("android.intent.action.VIEW",
                        Uri.parse(imgRedirct1));
        startActivity(viewIntent);
    }
    public void fun2(View view)
    {
        Log.d("function","fun2");

        Intent viewIntent =
                new Intent("android.intent.action.VIEW",
                        Uri.parse(imgRedirct2));
        startActivity(viewIntent);
    }
    public void fun3(View view)
    {
        Log.d("function","fun3");

        Intent viewIntent =
                new Intent("android.intent.action.VIEW",
                        Uri.parse(imgRedirct3));
        startActivity(viewIntent);
    }

}
