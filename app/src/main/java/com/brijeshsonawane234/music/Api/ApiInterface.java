package com.brijeshsonawane234.music.Api;

import com.brijeshsonawane234.music.Pojo.Json;
import com.brijeshsonawane234.music.Pojo.PostData;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface ApiInterface
{
    @POST("homepage/homebanner")
    Call<Json> postInfo(@Body PostData postData);

}
