package com.brijeshsonawane234.music.Pojo;

import java.io.Serializable;
import java.util.List;

public class Data implements Serializable
{
    String placeholder_name;
    String label;
    String id;
    String keyword;
    String redirect_url;

    List<Image> images;

    public Data(String id, String keyword, String redirect_url) {
        this.id = id;
        this.keyword = keyword;
        this.redirect_url = redirect_url;
    }

    public Data(String placeholder_name, List<Image> images) {
        this.placeholder_name = placeholder_name;
        this.images = images;
    }

    public Data(String placeholder_name, String label, List<Image> images) {
        this.placeholder_name = placeholder_name;
        this.label = label;
        this.images = images;
    }

    public String getPlaceholder_name() {
        return placeholder_name;
    }

    public void setPlaceholder_name(String placeholder_name) {
        this.placeholder_name = placeholder_name;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    public String getRedirect_url() {
        return redirect_url;
    }

    public void setRedirect_url(String redirect_url) {
        this.redirect_url = redirect_url;
    }

    public List<Image> getImages() {
        return images;
    }

    public void setImages(List<Image> images) {
        this.images = images;
    }
}
