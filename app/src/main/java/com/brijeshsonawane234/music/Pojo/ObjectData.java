package com.brijeshsonawane234.music.Pojo;

import java.io.Serializable;
import java.util.List;

public class ObjectData implements Serializable
{
    int template_id;
    int section_sequence;
    String section_header = "";
    String bg_color;

    List<Data> data;

    public ObjectData() {
    }

    public ObjectData(int template_id, int section_sequence, List<Data> data) {
        this.template_id = template_id;
        this.section_sequence = section_sequence;
        this.data = data;
    }


    public ObjectData(int template_id, int section_sequence, String section_header, List<Data> data) {
        this.template_id = template_id;
        this.section_sequence = section_sequence;
        this.section_header = section_header;
        this.data = data;
    }

    public ObjectData(int template_id, int section_sequence, String section_header, String bg_color, List<Data> data) {
        this.template_id = template_id;
        this.section_sequence = section_sequence;
        this.section_header = section_header;
        this.bg_color = bg_color;
        this.data = data;
    }

    public int getTemplate_id() {
        return template_id;
    }

    public void setTemplate_id(int template_id) {
        this.template_id = template_id;
    }

    public int getSection_sequence() {
        return section_sequence;
    }

    public void setSection_sequence(int section_sequence) {
        this.section_sequence = section_sequence;
    }

    public String getSection_header() {
        return section_header;
    }

    public void setSection_header(String section_header) {
        this.section_header = section_header;
    }

    public String getBg_color() {
        return bg_color;
    }

    public void setBg_color(String bg_color) {
        this.bg_color = bg_color;
    }

    public List<Data> getData() {
        return data;
    }

    public void setData(List<Data> data) {
        this.data = data;
    }
}
