package com.brijeshsonawane234.music.Pojo;

public class PostData
{
    Boolean offline_banners;
    String visitorId;

    Json json;

    public PostData(Boolean offline_banners, String visitorId, Json json) {
        this.offline_banners = offline_banners;
        this.visitorId = visitorId;
        this.json = json;
    }

    public PostData(Boolean offline_banners, String visitorId) {
        this.offline_banners = offline_banners;
        this.visitorId = visitorId;
    }

    public Boolean getOffline_banners() {
        return offline_banners;
    }

    public void setOffline_banners(Boolean offline_banners) {
        this.offline_banners = offline_banners;
    }

    public String getVisitorId() {
        return visitorId;
    }

    public void setVisitorId(String visitorId) {
        this.visitorId = visitorId;
    }

    public Json getJson() {
        return json;
    }

    public void setJson(Json json) {
        this.json = json;
    }
}
